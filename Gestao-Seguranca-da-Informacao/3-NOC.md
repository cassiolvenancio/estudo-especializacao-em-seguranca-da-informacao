[[_TOC_]]

# 1. NOC

## 1.1. O que é NOC?

O NOC (Network Operations Center) é uma estrutura com foco no 
gerenciamento centralizado das redes de dados públicas e privadas de uma organização 
(LAN, MAN e WAN). O seu principal objetivo é garantir a disponibilidade das 
comunicações da organização.

Nessa estrutura, especialistas com conhecimentos focados principalmente em administração de redes realizam a monitoração dos alertas gerados pelos ativos de TI. Toda a equipe deve atuar em um modelo com processos bem definidos, com o objetivo de manter o ambiente o mais estável possível.

O NOC monitora, analisa e controla a disponibilidade e o desempenho de todos 
os elementos presentes na rede incluindo roteadores, switches e computadores. As 
principais entregas do NOC estão relacionadas à gestão e ao controle de Tempo de latência, Consumo de recursos, incluindo largura de banda, cpu e memória, Desempenho dos diversos elementos que fazem parte da rede, Desvios e picos de uso, Capacidade do ambiente.

## 1.2. Motivo de uso

Para evitar que um usuário final identifique um erro na infraestrutura de TI antes da equipe responsável por este ambiente é aconselhável, muitas vezes, a implantação de uma estrutura de NOC.

O objetivo da central de operações de rede é monitorar toda a infraestrutura de TI para descobrir a existência de problemas no momento em que eles ocorram. Desta forma, é possível tomar providências para evitar que um erro de tecnologia gere perdas financeiras para a organização.

O Network Barometer Report 2016 indica que redes monitoradas têm resposta 69% mais rápida e tempo de reparo 32% menor em relação às que não são gerenciadas. Conforme o estudo, ⅓ dos incidentes são causados por configuração ou erro humano e ``75% das redes têm, pelo menos, uma vulnerabilidade de segurança.``

Todas essas falhas refletem no resultado financeiro. Afinal, custa muito caro alocar diversos profissionais para resolver problemas que ainda não se sabe quais são. Além disso, os problemas de rede não escolhem hora para aparecer e podem atrapalhar a produtividade da organização.
