[[_TOC_]]

# 1. Pilares da Segurança da Informação

Os conceitos abaixo são a base da defesa cibernética. O comprometimento de 
um ou mais destes elementos pode colocar em risco a sobrevivência da sua organização. 
Todas as atividades da área devem reduzir as chances de um ou mais pilares serem
quebrados. Questione-se: 
“Esta atividade de rotina ou este projeto reduz a exposição da minha 
organização?”. Se a resposta for não, é hora de repensar o que faz no seu dia a dia. Foco 
é essencial.

Os termos a baixo são conhecidos como Tríade CIA (Confidentiality, Integrity e 
Availability).

# 2. Confidencialidade

Garantir que a informação foi disponibilizada apenas para as pessoas autorizadas.
Esse conceito se relaciona às ações tomadas para assegurar que informações confidenciais e críticas não sejam roubadas dos sistemas organizacionais por meio de ciberataques, espionagem, entre outras práticas.

- Lembrando que a maior parte dos ataques cibernéticos são realizados por meio engenharia social, não adianta você ter um ambiente bem robusto de seguraça, se sua equipe não estiver treinada para incidentes de segurança.

- Imagina se alguém peça informações pessoais em nome de outra pessoa, e algum colaborador as fornece sem mesmo conferir nada. Não seria culpa do colaborador, seria culpa de uma política sem treinamento adequado de segurança.

# 3. Integridade

É garantir a precisão, consistência e confiabilidade dos 
dados durante todo o seu ciclo de vida, impedindo atualizações não 
autorizadas.

- Imagina alguém enviando código malicioso. É possivel caso não haja uma boa política de acessos.

# 4. Disponibilidade

Garantir que a informação está disponível para as pessoas autorizadas, conforme os padrões acordados.

Quando os dados da empresa se tornam indisponíveis, ela pode ter vários prejuízos graves, como interrupção de atividades que dependem deles e perda de vendas por não se ter acesso a informações comerciais. Também pode ocorrer pausa na produção por falta de sistemas operantes e nas comunicações internas e externas.












