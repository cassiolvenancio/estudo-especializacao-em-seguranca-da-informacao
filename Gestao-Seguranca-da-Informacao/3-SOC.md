[[_TOC_]]

# 1. SOC

## 1.1. O que é SOC?

O SOC (Security Operations Center) mantém o ambiente operacional,
garantindo sua atualização e disponibilidade.

O SOC representa a combinação perfeita de recursos humanos, processuais e tecnológicos para, juntos, formarem uma estrutura de gerenciamento forte para a segurança da informação. Essa estratégia pode ser aplicada a apenas uma empresa, individualmente, ou a várias, sendo oferecida como serviços por um ou mais provedores competentes.

A estrutura atua monitorando constantemente todos os recursos de segurança utilizados na empresa, como firewalls, IPs, antivírus, UTMs e anti-DDoS. Com mecanismos de correlacionamento, ela é capaz de cruzar dados sobre os eventos, proporcionando uma solução chamada de SIEM (Security Information and Event Management), detectando quase que instantaneamente as tentativas de invasão.

## 1.2. O que faz o SOC?

- Monitorar e analisar o tráfego de rede em busca de atividades maliciosas.
- Coordenar, com a equipe de inteligência e a equipe de resposta a incidentes, as atividades para garantir a - comunicação adequada de ameaças cibernéticas que podem afetar a segurança da rede.
- Redigir notificações de alerta de segurança.
- Adicionar, remover ou atualizar endereços IP e domínios.
- Compreender / diferenciar tentativas de invasão e alarmes falsos.
- Monitorar ameaças internas e executar a detecção do APT – Advanced Persistent Threats.
- Analisar vulnerabilidades de hardware e software.
- Responder a e-mails e telefonemas para endereçar notificações de incidentes cibernéticos.
- Investigar, documentar e relatar problemas de segurança.
- Realizar atividades de forense computacional, para evitar que ataques sofisticados causem graves problemas para redes corporativas.

## 1.3. Motivo de uso

O monitoramento 24/7 fornecido por um SOC oferece às organizações uma vantagem para se defender contra incidentes e intrusões, independentemente da origem, hora do dia ou tipo de ataque. Em outras palavras, ter um centro de operações de segurança ajuda as organizações a fechar lacunas e a se manter no controle das ameaças enfrentadas em seus ambientes de TI.
