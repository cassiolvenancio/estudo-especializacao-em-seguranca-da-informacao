[[_TOC_]]

# 1. Conceitos básicos de Segurança da Informação

## 1.2. O que é segurança da informação?

A segurança da informação é a área de conhecimento que agrega um conjunto 
de habilidades, recursos, processos e ferramentas necessárias para garantir a 
confidencialidade, integridade, disponibilidade, autenticidade e irretratabilidade das 
informações de uma organização ou indivíduo.

### 1.2.1. Consequências de uma segurança fragilizada

Considerando-se as perspectivas econômicas vigentes, as empresas vêm 
buscando o aumento da sua eficiência operacional limitando investimentos e reduzindo 
custos. Esse é um cenário particularmente desafiador para a segurança da informação.

O relatório VERIZON, 2020 Data Breach Investigation Report mostra que o 
tempo para uma falha de segurança ser explorada numa empresa tem ordem 
de grandeza de minutos. Por outro lado, o tempo para solução está na ordem 
de dias.

***Importante***

- ``Existem diversas ferramentas disponíveis on-line para a realização de ataques,
mesmo por pessoas sem grande domínio técnico.``

- As técnicas para a realização de ataques evoluem mais rápido que as técnicas 
de prevenção, deixando as empresas expostas.


## 1.3. O que é defesa cinernética?

A defesa cibernética tem por objetivo evitar que possíveis ataques cibernéticos 
tenham sucesso em um ambiente computacional. Ela contempla a prevenção, 
monitoramento e execução de contramedidas para antecipar, interromper e responder 
eventuais atacantes.

### 1.3.1. O que pode ser considerado um ataque cibernético?

Um ataque cibernético é uma tentativa de desabilitar computadores, roubar dados ou usar um sistema de computador violado para lançar ataques adicionais. Os criminosos virtuais usam diferentes métodos para lançar um ataque cibernético que incluem malware, phishing, ransomware, ataque man-in-the-middle ou outros métodos.

# 2. Jargões da segurança da informação

Link com alguns jargões utilizados na S.I. 

- https://github.com/john-goes/infosec-glossary
