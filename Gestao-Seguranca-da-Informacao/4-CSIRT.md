[[_TOC_]]

# 1. CSIRT

O CSIRT (Computer Security Incident Response Team) tem sua atuação 
direcionada para o tratamento de incidentes. É o grupo responsável por prevenir, atuar 
e gerenciar quaisquer incidentes de segurança. Ele também tem por função fazer o 
relacionamento com as diversas empresas que o suportam e com a mídia, quando 
necessário.

Um CSIRT pode oferecer diferentes tipos de serviço para os seus clientes. 
Tipicamente, eles contemplam atividades ligadas à análise, ao atendimento, ao suporte 
e/ou à coordenação de incidentes de segurança.

Existem diversos grupos de CSIRT atuando nos vários estados do Brasil e eles podem ser encontrados no site do Cert.br https://www.cert.br/csirts/brazil/. O CSIRT principal do Brasil é justamente o Cert.br, o Centro de Estudos, Respostas e Tratamentos de Incidentes de Segurança no Brasil.

Os CSIRTs nacionais trabalham em conjunto com os Centros de Análises de Redes e agrupam dados para traçar tendências de atividades relacionadas aos incidentes, buscando prevenir ameaças e antecipando os importantes alertas de segurança.
