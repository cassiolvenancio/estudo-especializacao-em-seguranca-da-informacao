[[_TOC_]]

# 1. O que são redes?

Uma rede de computadores pode ser criada quando 2 ou mais computadores, estão se comunicando entre si.
Logo uma rede de computadores, é como um "grupinho" de computadores ou quase como uma "familia" Ou você pode imaginar um circulo fechado e dentro dele, alguns computadores (nós), interligados.

# 2. Principais tipos de rede

Como dito, redes são um grupinho de computadores (nós) que se comunicam entre si, logo existe algumas que se diferenciam uma das outras.

A principal diferênça entre elas, são o tamanho que este grupo oferece, ou seja até aonde essa rede consegue se estender. 

## 2.1. LAN

Muito conhecida a LAN (Local Area Network), é uma rede local, muito comum em escolas e residências.
Esse rede se comunica através de cabos.

## 2.2. WLAN 

A WLAN (Wireless Local Area Network) é similar a LAN, só que é uma comunicação sem utilizar cabos, como Wifi.

## 2.3. MAN

A MAN (Metropolitan Area Network) é uma rede que se estende muito mais que uma LAN ou WLAN, podendo cobrir uma área de uma cidade inteira. 

## 2.4. WMAN

Esta é a versão sem fio da MAN, com um alcance de dezenas de quilômetros, sendo possível conectar redes de escritórios de uma mesma empresa ou de campus de universidades.

## 2.5. WAN 

A WAN (Wide Area Network)  Rede de Longa Distância, vai um pouco além da MAN e consegue abranger uma área maior, como um país ou até mesmo um continente.

## 2.6. WWAN

Com um alcance ainda maior, a WWAN, ou Rede de Longa Distância Sem Fio, alcança diversas partes do mundo. Justamente por isso, a WWAN está mais sujeita a ruídos.

