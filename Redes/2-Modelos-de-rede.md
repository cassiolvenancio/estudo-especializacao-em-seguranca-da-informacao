[[_TOC_]]

# 1. Modelo OSI

## 1.2. O que é modelo OSI?

O modelo Interconexão de Sistemas Abertos (OSI) é um modelo conceitual criado pela Organização Internacional de Normalização que permite que diversos sistemas de comunicação se comuniquem usando protocolos padronizados. Em poucas palavras, o OSI fornece um padrão para que diferentes sistemas de computadores possam se comunicar.

O modelo OSI pode ser visto como uma linguagem universal para Redes de computadores. É baseado no conceito de dividir um sistema de comunicação em sete camadas abstratas, empilhadas umas sobre as outras.

O modelo OSI, não é estritamente seguido na internet. ``O padrão é o TCP/IP``.

## 1.3. Estrutura, OSI

Modelo OSI segue uma estrutura em camadas, seguindo esta ordem.

PC - 01 se comunica com PC - 02

- 7. Camada de aplicativos - Cria uma mensagem através de um software. ``HTTP(s)``                             
            |                                               
            v          
- 6. Camada de apresentação  - Compacta a mensagem, e faz a criptografia, que agora e um dado. ``TLS``
            |                                                   
            v                                                
- 5. Camada de sessão - Estabele a conexão com a maquina PC - 02  ``NetBIOS``                                 
            |                                                   
            v                                                  
- 4. Camada de transporte - Prepara o envio do dado, utilizando algum   protocolo, no caso o ``TCP`` será usado                            
            |                                               
            v                                                  
- 3. Camada de Rede - Faz o roteamento para outra rede, pois o dado não está na mesma rede, vai ser utlizado o ``IP``, para encontrar o caminho da outra rede.                                    
            |                                                   
            v                                                       
- 2. Camada de enlace de dados - Faz a ligação e o controle de fluxo do dado. ``Ethernet``                       
            |                                                        
            v           
- 1. Camada física - Conexão via meios, vamos utilizar o ``Modem``.

PC - 02     |                                                        
            v    

- 1. Camada física - Dado recebido via ``Modem``.
            |                                                        
            v
- 2. Camada de enlace de dados - Verifica se a erros no dado e corrige ``Ethernet``                                  
            |                                                        
            v           
- 3. Camada de Rede  - Verifica para quem o dado quer chegar utilizando ``IP``
            |                                                   
            v      
- 4. Camada de transporte - Recebe o dado, utilizando o mesmo protocolo ``TCP``                           
            |                                               
            v     
- 5. Camada de sessão - Estabele a conexão ``NetBIOS``                           
            |                                                   
            v                     
- 6. Camada de apresentação - Descompacta o dado e faz a descriptografia ``TLS``                          
            |                                                   
            v             
- 7. Camada de aplicativos - Vê a mensagem recebida ``HTTP(s)``                                       


### 1.3.1. (1) Camada física

Essa camada inclui o equipamento físico envolvido na transferência de dados, como cabos e comutadores. Essa também é a camada em que os dados são convertidos em um fluxo de bits, que é uma sequência de 1s e 0s. A camada física de ambos os dispositivos também precisa aceitar de comum acordo uma convenção de sinal para que se possa distinguir os 1s dos 0s em ambos os dispositivos.

### 1.3.2. (2) Camada de enlace de dados

A camada de enlace de dados é muito semelhante à camada de rede, a não ser pelo fato de que a camada de enlace de dados facilita a transferência de dados entre dois dispositivos na MESMA rede.

A camada de enlace de dados pega os pacotes da camada de rede e os divide em pedaços menores denominados "quadros". Como a camada de rede, a camada de enlace de dados também é responsável pelo controle de fluxo e pelo controle de erros na comunicação intrarrede (a camada de transporte faz o controle de fluxo e o controle de erros para comunicações inter-rede).

### 1.3.3. (3) Camada de Rede

A camada de rede é responsável por facilitar a transferência de dados entre duas redes diferentes. Se os dois dispositivos que estão se comunicando estiverem na mesma rede, a camada de rede será desnecessária. A camada de rede divide os segmentos da camada de transporte em unidades menores denominadas pacotes no dispositivo remetente e remonta esses pacotes no dispositivo receptor. A camada de rede também encontra o melhor caminho físico para que os dados cheguem ao seu destino, o que é conhecido como "roteamento".

### 1.3.4. (4) Camada de transporte

Se na camada um temos as estradas e os caminhos que os dados percorrem, na camada quatro temos os caminhões e os carteiros.

É esta camada que garante o envio e o recebimento dos pacotes vindos da camada 3. Ela gerencia o transporte dos pacotes para garantir o sucesso no envio e no recebimento de dados.

Esta camada lida muito com a qualidade do serviço para que os dados sejam entregues com consistência, isto é, sem erros ou duplicações. Porém nem todos os protocolos desta camada garantem a entrega da mensagem.

Protocolos muito comuns dessa camada são os protocolos TCP em UDP. O primeiro garante a entrega da mensagem, diferente do segundo. Por não garantir a entrega da mensagem, o protocolo UDP é um pouco mais rápido que o TCP.

### 1.3.5. (5) Sessão

Está camada é responsável por estabelecer e encerrar a conexão entre hosts. É ela quem inicia e sincroniza os hosts.

Além de realizar o estabelecimento das sessões, esta camada também provém algum suporte a elas, como registros de log e realizando tarefas de segurança.

### 1.3.6. (6) Apresentação

Está é a camada responsável por fazer a tradução dos dados para que a próxima camada os use. Nesta camada temos a conversão de códigos para caracteres, a conversão e compactação dos dados, além da criptografia desses dados, caso necessite.

### 1.3.7. (7) Aplicação

A última camada do modelo OSI é a camada para consumir os dados. Nesta camada temos os programas que garantem a interação humano-máquina. Nela conseguimos enviar e-mails, transferir arquivos, acessar websites.
